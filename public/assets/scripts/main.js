const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;

const REQUIRED_ERROR_MESSAGE = 'Please provide an email';
const NOT_A_MAIL_ERROR_MESSAGE = 'Please provide a valid email';

const DATA_ERROR_ATTR = 'data-error';

window.onload = () => {
  const emailInput = document.getElementById('email');
  const inputContainer = document.getElementById('email-input-container');

  document.getElementById('form')
    .addEventListener('submit', (event) => {
      event.preventDefault();


      if (!emailInput.value) {
        inputContainer.setAttribute(DATA_ERROR_ATTR, REQUIRED_ERROR_MESSAGE);
        return;
      }

      if (!EMAIL_REGEXP.test(emailInput.value)) {
        inputContainer.setAttribute(DATA_ERROR_ATTR, NOT_A_MAIL_ERROR_MESSAGE);
        return;
      }

      inputContainer.removeAttribute(DATA_ERROR_ATTR);
    });
}
